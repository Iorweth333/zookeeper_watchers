import org.apache.zookeeper.*;
import org.apache.zookeeper.AsyncCallback.StatCallback;
import org.apache.zookeeper.AsyncCallback.Children2Callback;
import org.apache.zookeeper.data.Stat;

import java.io.*;
import java.util.Arrays;
import java.util.List;

//https://zookeeper.apache.org/doc/r3.1.2/javaExample.html

public class Executor
        implements Watcher, Runnable, StatCallback, Children2Callback {
    String znode;
    ZooKeeper zk;
    String exec;
    Process child;
    boolean dead;

    public Executor (String connectString, String znode, String exec) throws KeeperException, IOException {
        this.exec = exec;
        this.znode = znode;
        zk = new ZooKeeper(connectString, 3000, this);
        zk.exists(znode, true, this, null);
        zk.getChildren(znode, true, this, null);
    }

    /**
     @param args
     */
    public static void main (String[] args) {
        String connectString = "127.0.0.1:2181";
        String znode = "/z";
        String exec = "notepad";
        try {
            new Executor(connectString, znode, exec).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    /***************************************************************************
     * We do process any events ourselves, we just need to forward them on.
     *
     //   * @see org.apache.zookeeper.Watcher#process(org.apache.zookeeper.proto.WatcherEvent)
     */
    public void process (WatchedEvent event) {
        String path = event.getPath();
        switch (event.getType()) {
            case None:
                switch (event.getState()) {
                    case SyncConnected:
                        // In this particular example we don't need to do anything
                        // here - watches are automatically re-registered with
                        // server and any watches triggered while the client was
                        // disconnected will be delivered (in order of course)
                        break;
                    case Expired:
                        // It's all over
                        dead = true;
                        closing(KeeperException.Code.SessionExpired);
                        break;
                }
                break;

            case NodeCreated:
                System.out.println("Starting child");
                try {
                    child = Runtime.getRuntime().exec(exec);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                zk.exists(znode, true, this, null);
                zk.getChildren(znode, true, this, null);
                break;


            case NodeDeleted:
                System.out.println("Stopping child");
                child.destroy();
                try {
                    child.waitFor();
                    System.out.println("Process stopped with exit value: " + child.exitValue());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                zk.exists(znode, true, this, null);
                zk.getChildren(znode, true, this, null);
                break;

            case NodeChildrenChanged:
                try {
                    System.out.println("Number of children of given znode: " + zk.getChildren(znode, true).size());
                } catch (KeeperException e) {
                    System.out.println("Node " + e.getPath() + " doesn't exist any more.");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                zk.exists(znode, true, this, null);
                zk.getChildren(znode, true, this, null);
                break;

            default:
                if (path != null && path.equals(znode)) {
                    // Something has changed on the node, let's find out
                    zk.exists(znode, true, this, null);
                    zk.getChildren(znode, true, this, null);
                }



        }

    }

    public void run () {
        try {
            String line;
            String path = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("q to quit, p + path to print tree");
            while (true) {
                System.out.println(">");
                line = br.readLine();
                if (line.startsWith("q") || line.startsWith("e")) {
                    break;
                }
                if (line.startsWith("p")) {
                    path = extractPathFromLine(line);
                }
                printTree(path, 0);
            }
        } catch (Exception e) {
        }
    }

    private String extractPathFromLine (String line) {
        int spacePos = line.indexOf(' ');
        String path = line.substring(spacePos + 1);
        return path.trim();
    }

    private void printTree (String path, int indentation) {
        if (path != null) {
            try {
                String current = extractCurrentFromPath(path);
                for (int i = 0; i < indentation; i++) {     //add indentation
                    System.out.print(" ");
                }
                System.out.println(current);
                List<String> children = zk.getChildren(path, false);
                //TODO
                for (String child : children){
                    String subpath = path + "/" + child;
                    printTree(subpath, indentation + 2);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String extractCurrentFromPath (String path) {
        int lastSlashPos = path.lastIndexOf('/');
        if (lastSlashPos == -1) {
            lastSlashPos = path.lastIndexOf('\\');
        }
        return path.substring(lastSlashPos + 1);

    }

    public void closing (int rc) {
        synchronized (this) {
            notifyAll();
        }
    }

    @Override
    public void processResult (int rc, String s, Object o, Stat stat) {
        boolean exists;
        switch (rc) {
            case KeeperException.Code.Ok:
                exists = true;
                break;
            case KeeperException.Code.NoNode:
                exists = false;
                break;
            case KeeperException.Code.SessionExpired:
            case KeeperException.Code.NoAuth:
                dead = true;
                closing(rc);
                return;
            default:
                // Retry errors
                zk.exists(znode, true, this, null);
                return;
        }

        if (!exists) {
            System.out.println(znode + " doesn't exist");
        }

    }

    @Override
    public void processResult (int i, String s, Object o, List<String> list, Stat stat) {
    }


}